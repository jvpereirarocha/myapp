document.newPublisher.onsubmit = async e => {
    e.preventDefault()
    const form = e.target
    const urlBase = 'http://localhost:8000'
    const data = new FormData(form)
    let urlRedirect = form.getAttribute('redirect-to')

    console.log('url is: ', urlRedirect)

    const options = {
        method: form.method,
        body: new URLSearchParams(data)
    }

    fetch(form.action, options)
    .then(response => {
        if (response.status == 200) {
            return response.json()
        }
        else {
            return Promise.reject({response})
        }
    })
    .then(json => {
        console.log(json)
    })
    .catch(e => {
        alert = document.getElementById('error')
        alert.innerHTML = e
    })
}
