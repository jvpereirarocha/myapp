from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views import View
from .models import Publisher, Author, Book
from django.http import HttpResponse, JsonResponse
from .forms import PublisherCreateForm
from django.contrib import messages
from django.core import serializers


class RegisterView(View):
    pass


class AuthLoginView(View):
    pass


class AuthLogoutView(View):
    pass


# Index view
class IndexView(View):
    def get(self, request, *args, **kwargs):
        title = 'Welcome to the Home Page '
        message = 'Explore the navigate options and choose any'
        footer = 'Learn more about this application choosing any feature'
        context = {
            'title': title,
            'message': message,
            'footer': footer
        }
        return render(request, 'index.html', context)


# Publisher views
class PublisherListView(View):

    def get(self, request, *args, **kwargs):
        queryset = Publisher.objects.all()
        name = request.GET.get('publisher')
        if name:
            queryset = queryset.filter(name__iexact=name)
        context = {
            'publishers': queryset,
            'count': queryset.count()
        }
        return render(request, 'publisher/list.html', context)


class PublisherCreateView(CreateView):
    template_name = 'publisher/create.html'
    model = Publisher
    form_class = PublisherCreateForm
    context_object_name = 'form'
    success_url = '/publisher/list'

    def form_valid(self, form):
        if self.request.method == 'POST' and self.request.is_ajax:
            instance = form.save()
            data = serializers.serialize('json', [instance, ])
            return JsonResponse({'data': data})

    def form_invalid(self, form):
        if self.request.method == 'POST' and self.request.is_ajax:
            error = 'Form is invalid. Try again'
            print(error)
            return JsonResponse({'error': error})


class PublisherUpdateView(UpdateView):
    pass


class PublisherDeleteView(DeleteView):
    pass


# Author views
class AuthorListView(View):
    def get(self, request, *args, **kwargs):
        author = request.GET.get('author')
        email = request.GET.get('email')
        queryset = Author.objects.order_by('name')
        if author:
            queryset = queryset.filter(name__iexact=author)
        if email:
            queryset = queryset.filter(email__iexact=email)

        context = {
            'authors': queryset,
            'count': queryset.count()
        }
        return render(request, 'author/list.html', context)


class AuthorCreateView(CreateView):
    pass


class AuthorUpdateView(UpdateView):
    pass


class AuthorDeleteView(DeleteView):
    pass


# Book views
class BookListView(View):

    def get(self, request, *args, **kwargs):
        queryset = Book.objects.order_by('title')
        author = request.GET.get('author')
        publisher = request.GET.get('publisher')
        if author:
            queryset = queryset.filter(authors__name__iexact=author)
        if publisher:
            queryset = queryset.filter(publisher__name__iexact=publisher)
        context = {
            'books': queryset,
            'count': queryset.count()
        }
        return render(request, 'book/list.html', context)


class BookCreateView(CreateView):
    pass


class BookUpdateView(UpdateView):
    pass


class BookDeleteView(DeleteView):
    pass
