from django.db import models


class Publisher(models.Model):
    name = models.CharField(max_length=100, verbose_name='Name')
    address = models.CharField(max_length=50, verbose_name='Address')
    city = models.CharField(max_length=50, verbose_name='City')
    state_province = models.CharField(max_length=30, verbose_name='State')
    country = models.CharField(max_length=50, verbose_name='Country')
    website = models.URLField(null=True, blank=True, verbose_name='Website')

    class Meta:
        ordering = ['-name']
        verbose_name = 'Publisher'
        verbose_name_plural = 'Publishers'

    def __str__(self):
        return self.name


class Author(models.Model):
    salutation = models.CharField(max_length=10, verbose_name='Salutation')
    name = models.CharField(max_length=200, verbose_name='Name')
    email = models.EmailField(max_length=100, verbose_name='E-mail')
    headshot = models.ImageField(verbose_name='Headshot', upload_to='headshots')

    class Meta:
        ordering = ['-name']
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=100, verbose_name='Title')
    authors = models.ManyToManyField(Author, related_name='books', verbose_name='Authors')
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE,
                                  related_name='books', verbose_name='Publisher')
    publication_date = models.DateField(verbose_name='Publication Date')

    class Meta:
        ordering = ['-publication_date']
        verbose_name = 'Book'
        verbose_name_plural = 'Books'

    def __str__(self):
        return self.title
