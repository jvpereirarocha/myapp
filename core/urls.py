from django.urls import path, include
from .views import (IndexView, AuthLoginView, AuthLogoutView, RegisterView,
                    PublisherListView, PublisherCreateView, PublisherUpdateView, PublisherDeleteView,
                    AuthorListView, AuthorCreateView, AuthorUpdateView, AuthorDeleteView,
                    BookListView, BookCreateView, BookUpdateView, BookDeleteView)

app_name = 'core'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('login/', AuthLoginView.as_view(), name='login'),
    path('logout/', AuthLogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('publisher/list/', PublisherListView.as_view(), name='publisher_list'),
    path('publisher/new/', PublisherCreateView.as_view(), name='publisher_new'),
    path('publisher/detail/<int:pk>', PublisherUpdateView.as_view(), name='publisher_update'),
    path('publisher/delete/<int:pk>', PublisherDeleteView.as_view(), name='publisher_delete'),
    path('author/list/', AuthorListView.as_view(), name='author_list'),
    path('author/new/', AuthorCreateView.as_view(), name='author_new'),
    path('author/detail/<int:pk>', AuthorUpdateView.as_view(), name='author_update'),
    path('author/delete/<int:pk>', AuthorDeleteView.as_view(), name='author_delete'),
    path('book/list/', BookListView.as_view(), name='book_list'),
    path('book/new/', BookCreateView.as_view(), name='book_new'),
    path('book/detail/<int:pk>', BookUpdateView.as_view(), name='book_update'),
    path('book/delete/<int:pk>', BookDeleteView.as_view(), name='book_delete'),
]
