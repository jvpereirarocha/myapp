from .models import Publisher
from django import forms


class PublisherCreateForm(forms.ModelForm):

    class Meta:
        model = Publisher
        fields = ['name', 'address', 'city', 'state_province', 'country', 'website']
